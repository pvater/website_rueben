<?php

// Daten als Array laden (Zeichensatz UTF-8!)
$array = unserialize(file_get_contents("https://www.schachbund.de/php/dewis/verein.php?zps=F1521&format=array"));

// Array für die Sortierung umbauen (nur relevante Spalten)
foreach($array as $key => $value)
{
  $id[$key] = $value["id"];
  $nachname[$key] = $value["nachname"];
  $vorname[$key] = $value["vorname"];
  $dwz[$key] = $value["dwz"];
  $dwzindex[$key] = $value["dwzindex"];
}

// Nach DWZ und Index sortieren
array_multisort($dwz,SORT_DESC,SORT_NUMERIC,$dwzindex,SORT_DESC,SORT_NUMERIC,$id,$nachname,$vorname);

// Liste ausgeben
echo "<table class=\"table\">\n";
echo "<tr>\n";
echo "<th>Pl.</th>\n";
echo "<th>Name</th>\n";
echo "<th>DWZ</th>\n";
echo "<th>Index</th>\n";
echo "</tr>\n";
for($x=0;$x<count($dwz);$x++)
{
  $platz = $x+1;
  echo "<tr>\n";
  echo "<td>$platz</td>\n";
  echo "<td><a href=\"https://www.schachbund.de/spieler.html?pkz=".$id[$x]."\" target=\"_blank\">".$nachname[$x].",".$vorname[$x]."</a></td>\n";
  echo "<td>$dwz[$x]</td>\n";
  echo "<td>$dwzindex[$x]</td>\n";
  echo "</tr>\n";
}
echo "</table>\n";

?>
