<?php

function get_svs_tabelle($year, $id) {
  /* $tabelle=file_get_contents("https://svs.portal64.de/ergebnisse/show/2021/1979/tabelle/lang/plain/"); */
  $tabelle=file_get_contents("https://svs.portal64.de/ergebnisse/show/$year/$id/tabelle/");
  $tabelle = mb_convert_encoding($tabelle, 'UTF-8', "ISO-8859-1");
  $tabelle=preg_replace('/portal64\_ergebnistabelle/','table',$tabelle);
  //$tabelle=preg_match('/<table.*table>/s',$tabelle,$out);
  $tabelle=preg_match('/<h1.*table>/s',$tabelle,$out);
  echo $out[0];
}

function get_svs_termin($year, $id) {
  $termin=file_get_contents("https://svs.portal64.de/ergebnisse/show/$year/$id/termin/plain/");
  $termin = mb_convert_encoding($termin, 'UTF-8', "ISO-8859-1");
  //$termin=preg_replace('/<table class="termine" border="1">/','<table class="table">',$termin);
  //$termin=preg_replace('/<td/','<td class="w-25"',$termin);
  $termin=preg_replace(array('/<td> \- <\/td>/', '/<table class="termine" border="1">/', '/<td>/', '/<td align="center">/'), array('', '<table class="table">', '<td class="col-sm-3">', '<td class="col-sm-1" align="center">'),$termin);
  echo "<h1>Terminübersicht</h1>";
  echo $termin;
}

function get_svs($year, $id) {
  get_svs_tabelle($year, $id);
  get_svs_termin($year, $id);
}

?>
