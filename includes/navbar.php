<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Main navigation">
  <div class="container-fluid">
    <a class="navbar-brand" href=".">SC Rote Rüben Leipzig e.V.</a>

    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/dwz.php">DWZ-Liste</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/kontakt.php">Kontakt</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-bs-toggle="dropdown" aria-expanded="false">Mannschaften</a>
          <ul class="dropdown-menu" aria-labelledby="dropdown01">
            <li><a class="dropdown-item" href="/mannschaft.php?id=1949">1. Mannschaft</a></li>
            <li><a class="dropdown-item" href="/mannschaft.php?id=1981">2. Mannschaft</a></li>
            <li><a class="dropdown-item" href="/mannschaft.php?id=1979">3. Mannschaft</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>

