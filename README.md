# Website der SC Rote Rueben Leipzig e.V.

Benutzt Bootstrap 5 fuer CSS und Javascript templating.

# Changelog

## 0.1, 2022-07-09

* initial working version - nice navigation bar, contact info, scripts to get+embed dwz-liste & team-info of current season

## 0.2, 2022-07-11

* include bootstrap-files from local copies, instead of jsdelivr.net
* modularize some code (common header+footer, generate all mannschaft-pages from one template)
